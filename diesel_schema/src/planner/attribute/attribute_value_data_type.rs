use diesel::deserialize::{self, FromSql};
use diesel::pg::{Pg, PgValue};
use diesel::serialize::{self, IsNull, Output, ToSql};
use serde::{Deserialize, Serialize};
use std::io::Write;

#[derive(Debug, FromSqlRow, AsExpression, Serialize, Deserialize, PartialEq, Eq)]
#[diesel(sql_type = crate::schema::sql_types::AttributeValueDataType)]
pub enum AttributeValueDataType {
    Text,
    RichText,
    Number,
    Color,
    Date,
    Boolean,
    File,
    Currency,
    Table,
    List,
    Range,
}

impl ToSql<crate::schema::sql_types::AttributeValueDataType, Pg> for AttributeValueDataType {
    fn to_sql<'b>(&'b self, out: &mut Output<'b, '_, Pg>) -> serialize::Result {
        match *self {
            AttributeValueDataType::Text => out.write_all(b"text")?,
            AttributeValueDataType::RichText => out.write_all(b"rich_text")?,
            AttributeValueDataType::Number => out.write_all(b"number")?,
            AttributeValueDataType::Color => out.write_all(b"color")?,
            AttributeValueDataType::Date => out.write_all(b"date")?,
            AttributeValueDataType::Boolean => out.write_all(b"boolean")?,
            AttributeValueDataType::File => out.write_all(b"file")?,
            AttributeValueDataType::Currency => out.write_all(b"currency")?,
            AttributeValueDataType::Table => out.write_all(b"table")?,
            AttributeValueDataType::List => out.write_all(b"list")?,
            AttributeValueDataType::Range => out.write_all(b"range")?,
        }

        Ok(IsNull::No)
    }
}

impl FromSql<crate::schema::sql_types::AttributeValueDataType, Pg> for AttributeValueDataType {
    fn from_sql(bytes: PgValue) -> deserialize::Result<Self> {
        match bytes.as_bytes() {
            b"text" => Ok(AttributeValueDataType::Text),
            b"rich_text" => Ok(AttributeValueDataType::RichText),
            b"number" => Ok(AttributeValueDataType::Number),
            b"color" => Ok(AttributeValueDataType::Color),
            b"date" => Ok(AttributeValueDataType::Date),
            b"boolean" => Ok(AttributeValueDataType::Boolean),
            b"file" => Ok(AttributeValueDataType::File),
            b"currency" => Ok(AttributeValueDataType::Currency),
            b"table" => Ok(AttributeValueDataType::Table),
            b"list" => Ok(AttributeValueDataType::List),
            b"range" => Ok(AttributeValueDataType::Range),
            _ => Err("Unrecognized enum variant".into()),
        }
    }
}
