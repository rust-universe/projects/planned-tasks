use super::attribute_value::AttributeValue;
use crate::schema::*;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Identifiable, Queryable, Insertable, Associations, Debug)]
#[diesel(belongs_to(AttributeValue))]
pub struct AttrValidation {
    pub id: i32,
    pub min: Option<i16>,
    pub max: Option<i16>,
    pub regex: Option<String>,
    pub attribute_value_id: Option<i32>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}
