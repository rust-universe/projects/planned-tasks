use crate::attribute::Attribute;
use crate::planner::task::Task;
use crate::schema::*;
use chrono::{DateTime, Utc};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Identifiable, Queryable, Insertable, Associations, Debug)]
#[diesel(belongs_to(Attribute))]
#[diesel(belongs_to(Task))]
pub struct AttributeValue {
    pub id: i32,
    pub text: Option<String>,
    pub large_text: Option<String>,
    pub number: Option<i64>,
    pub choice: Option<bool>,
    pub binary_data: Option<Vec<u8>>,
    pub task: Option<i32>,
    pub attribute_id: Option<i32>,
    pub task_id: Option<i32>,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}
