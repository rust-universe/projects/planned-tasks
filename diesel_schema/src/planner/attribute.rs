pub mod attr_validation;
pub mod attribute_value;
mod attribute_value_data_type;

use crate::entity_error::EntityError;
use crate::schema::attributes;
use crate::schema::attributes::columns::created_at as col_created_at;
use crate::schema::attributes::columns::description as col_description;
use crate::schema::attributes::columns::display_text as col_display_text;
use crate::schema::attributes::columns::is_template as col_is_template;
use crate::schema::attributes::columns::updated_at as col_updated_at;

use chrono::{DateTime, Utc};
use diesel::prelude::*;
use log::{debug, error};
use serde::{Deserialize, Serialize};

use mod_common::PaginationQuery;
use mod_common::PaginationResult;

use crate::diesel_extension::Paginate;
pub use attribute_value_data_type::AttributeValueDataType;

#[derive(Serialize, Deserialize, Identifiable, Queryable, Insertable, Debug)]
pub struct Attribute {
    pub id: i32,
    pub display_text: String,
    pub description: Option<String>,
    pub data_type: AttributeValueDataType,
    pub is_template: bool,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

#[derive(Serialize, Insertable)]
#[diesel(table_name = attributes)]
pub struct NewAttribute {
    pub display_text: String,
    pub description: Option<String>,
    pub data_type: AttributeValueDataType,
}

pub struct SearchAttributeModel {
    pub id: Option<i32>,
    pub display_text: Option<String>,
    pub description: Option<String>,
    pub data_type: Option<AttributeValueDataType>,
    pub is_template: Option<bool>,
    pub created_at: Option<DateTime<Utc>>,
    pub updated_at: Option<DateTime<Utc>>,
}

type AllColumns = (
    attributes::id,
    attributes::display_text,
    attributes::description,
    attributes::data_type,
    attributes::is_template,
    attributes::created_at,
    attributes::updated_at,
);

const ALL_COLUMNS: AllColumns = (
    attributes::id,
    attributes::display_text,
    attributes::description,
    attributes::data_type,
    attributes::is_template,
    attributes::created_at,
    attributes::updated_at,
);

type SelectAll = diesel::dsl::Select<attributes::table, AllColumns>;
type AllTemplateFiltered = diesel::dsl::Filter<SelectAll, diesel::dsl::Eq<col_is_template, bool>>;
type AllStringFiltered<T> = diesel::dsl::Filter<SelectAll, crate::helper_types::StringILike<T>>;
type AllStringEq<T> = diesel::dsl::Filter<SelectAll, crate::helper_types::StringEq<T>>;
type AllDateFilteredGt<T> = diesel::dsl::Filter<SelectAll, diesel::dsl::Gt<T, DateTime<Utc>>>;
type AllDateFilteredLt<T> = diesel::dsl::Filter<SelectAll, diesel::dsl::Lt<T, DateTime<Utc>>>;

/// Create query for selecting all the columns from the attributes table.
pub fn all() -> SelectAll {
    attributes::table.select(ALL_COLUMNS)
}

/// Create a query for fetching all the template attributes from DB.
pub fn all_templates() -> AllTemplateFiltered {
    all().filter(col_is_template.eq(true))
}

/// Return a query which can fetch all the attributes which are not marked as template attributes
pub fn all_non_template() -> AllTemplateFiltered {
    all().filter(col_is_template.eq(false))
}

/// Create a query to find a single record based on the ID passed to this function.
pub fn find_by_id(id: i32) -> diesel::dsl::Find<attributes::table, i32> {
    attributes::table.find(id)
}

/// Create a query to find all the attributes matching with
/// the display text passed to this function.
pub fn find_by_matching_display_text(display_text: &str) -> AllStringFiltered<col_display_text> {
    let like_clause_val = format!("%{}%", display_text);
    let ilike_clause = col_display_text.ilike(like_clause_val);

    all().filter(ilike_clause)
}

pub fn find_by_display_text(display_text: &str) -> AllStringEq<col_display_text> {
    all().filter(col_display_text.eq(display_text.to_string()))
}

pub fn find_by_matching_description(description: &str) -> AllStringFiltered<col_description> {
    let like_clause_val = format!("%{}%", description);
    let ilike_clause = col_description.ilike(like_clause_val);

    all().filter(ilike_clause)
}

pub fn find_by_description(description: &str) -> AllStringEq<col_description> {
    all().filter(col_description.eq(description.to_string()))
}

pub fn created_before(created_at: DateTime<Utc>) -> AllDateFilteredLt<col_created_at> {
    all().filter(col_created_at.lt(created_at))
}

pub fn created_after(created_at: DateTime<Utc>) -> AllDateFilteredGt<col_created_at> {
    all().filter(col_created_at.gt(created_at))
}

pub fn updated_before(updated_at: DateTime<Utc>) -> AllDateFilteredLt<col_updated_at> {
    all().filter(col_updated_at.lt(updated_at))
}

pub fn updated_after(updated_at: DateTime<Utc>) -> AllDateFilteredGt<col_updated_at> {
    all().filter(col_updated_at.gt(updated_at))
}

impl Attribute {
    pub fn create(
        conn: &mut PgConnection,
        attribute_details: NewAttribute,
        force_create: bool,
    ) -> Result<Attribute, Box<EntityError>> {
        // Check for attribute with exact display text match
        let result: Result<Attribute, diesel::result::Error> =
            find_by_display_text(&attribute_details.display_text).get_result::<Attribute>(conn);
        if !force_create && result.is_ok() {
            debug!("Attribute found with attribute ID: {}", result.unwrap().id);
            return Err(Box::new(EntityError::RecordAlreadyExists));
        }

        match diesel::insert_into(attributes::table)
            .values(attribute_details)
            .get_result(conn)
        {
            Ok(query_result) => Ok(query_result),
            Err(diesel_error) => {
                error!("Error: {}", diesel_error);
                Err(Box::new(EntityError::RecordCreationFailed))
            }
        }
    }

    pub fn find_by_id(
        conn: &mut PgConnection,
        attribute_id: i32,
    ) -> Result<Attribute, Box<EntityError>> {
        match find_by_id(attribute_id).first(conn) {
            Ok(attribute_found) => Ok(attribute_found),
            Err(diesel_error) => match &diesel_error {
                diesel::result::Error::NotFound => Err(Box::new(EntityError::RecordNotFound)),
                diesel::result::Error::DatabaseError(_error_kind, _db_error_info) => {
                    error!("Error: {}", diesel_error);
                    Err(Box::new(EntityError::DatabaseError))
                }
                _ => {
                    error!("Error: {}", diesel_error);
                    Err(Box::new(EntityError::Unknown(diesel_error)))
                }
            },
        }
    }

    pub fn get_all(
        conn: &mut PgConnection,
        pagination_query: PaginationQuery,
    ) -> Result<PaginationResult<Attribute>, Box<EntityError>> {
        let query_result = all()
            .paginate(pagination_query.page.try_into().unwrap())
            .per_page(pagination_query.per_page.try_into().unwrap())
            .load_and_count_pages::<Attribute>(conn);

        match query_result {
            Ok(attr_and_page_count) => Ok(PaginationResult {
                items: attr_and_page_count.0,
                total_pages: attr_and_page_count.1,
                query: pagination_query,
            }),
            Err(diesel_error) => match &diesel_error {
                diesel::result::Error::DatabaseError(_error_kind, _db_error_info) => {
                    error!("{}", diesel_error);
                    Err(Box::new(EntityError::DatabaseError))
                }
                _ => {
                    error!("{}", diesel_error);
                    Err(Box::new(EntityError::Unknown(diesel_error)))
                }
            },
        }
    }

    pub fn delete_record(
        conn: &mut PgConnection,
        attribute_id: i32,
    ) -> Result<bool, Box<EntityError>> {
        let num_deleted = diesel::delete(find_by_id(attribute_id))
            .execute(conn)
            .expect("Error deleting an attribute");

        match num_deleted {
            1 => Ok(true),
            _ => Err(Box::new(EntityError::RecordDeletionFailed)),
        }
    }

    pub fn delete_all_records(conn: &mut PgConnection) -> Result<bool, Box<EntityError>> {
        let count: i64 = attributes::dsl::attributes
            .count()
            .get_result(conn)
            .unwrap();
        let num_deleted: usize = diesel::delete(attributes::dsl::attributes)
            .execute(conn)
            .expect("Error clearing all records from table.");

        match num_deleted {
            deletion_count if deletion_count as i64 == count => Ok(true),
            _ => Err(Box::new(EntityError::RecordDeletionFailed)),
        }
    }
}
