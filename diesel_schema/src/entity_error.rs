use derive_more::{Display, Error};
use diesel::result::Error;

#[derive(Debug, Display, Error)]
pub enum EntityError {
    #[display(fmt = "RecordAlreadyExists")]
    RecordAlreadyExists,
    #[display(fmt = "RecordCreationFailed")]
    RecordCreationFailed,
    #[display(fmt = "RecordNotFound")]
    RecordNotFound,
    #[display(fmt = "DatabaseError")]
    DatabaseError,
    #[display(fmt = "Unknown")]
    Unknown(Error),
    #[display(fmt = "RecordDeletionFailed")]
    RecordDeletionFailed,
}

impl From<Error> for EntityError {
    fn from(err: Error) -> Self {
        EntityError::Unknown(err)
    }
}

impl From<Error> for Box<EntityError> {
    fn from(err: Error) -> Self {
        Box::new(EntityError::Unknown(err))
    }
}
