#![allow(unused)]
#![allow(clippy::all)]

// @generated automatically by Diesel CLI.

pub mod sql_types {
    #[derive(diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "attribute_value_data_type"))]
    pub struct AttributeValueDataType;
}

diesel::table! {
    use diesel::sql_types::*;

    attr_validations (id) {
        id -> Int4,
        min -> Nullable<Int2>,
        max -> Nullable<Int2>,
        regex -> Nullable<Varchar>,
        attribute_value_id -> Nullable<Int4>,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

diesel::table! {
    use diesel::sql_types::*;

    attribute_values (id) {
        id -> Int4,
        text -> Nullable<Varchar>,
        large_text -> Nullable<Text>,
        number -> Nullable<Int8>,
        choice -> Nullable<Bool>,
        binary_data -> Nullable<Bytea>,
        task -> Nullable<Int4>,
        attribute_id -> Nullable<Int4>,
        task_id -> Nullable<Int4>,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::AttributeValueDataType;

    attributes (id) {
        id -> Int4,
        display_text -> Varchar,
        description -> Nullable<Text>,
        data_type -> AttributeValueDataType,
        is_template -> Bool,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

diesel::table! {
    use diesel::sql_types::*;

    tasks (id) {
        id -> Int4,
        title -> Varchar,
        description -> Nullable<Text>,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

diesel::joinable!(attr_validations -> attribute_values (attribute_value_id));
diesel::joinable!(attribute_values -> attributes (attribute_id));

diesel::allow_tables_to_appear_in_same_query!(
    attr_validations,
    attribute_values,
    attributes,
    tasks,
);
