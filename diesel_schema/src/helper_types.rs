// Types available only inside the crate.
pub(crate) type StringILike<T> = diesel::helper_types::ILike<T, String>;
pub(crate) type StringEq<T> = diesel::dsl::Eq<T, String>;
