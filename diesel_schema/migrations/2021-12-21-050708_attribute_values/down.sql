-- CASCADE in drop table query will take care of removing foreign key constraints as well

DROP TABLE attribute_values CASCADE;
DROP TABLE attr_validations CASCADE;
