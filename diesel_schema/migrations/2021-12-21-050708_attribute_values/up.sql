CREATE TABLE IF NOT EXISTS attribute_values (
    id SERIAL PRIMARY KEY,
    text VARCHAR(255),
    large_text TEXT,
    number BIGINT,
    choice BOOLEAN,
    binary_data bytea,
    task INT,
    attribute_id INT,
    task_id INT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT fk_tasks_attr
        FOREIGN KEY(task)
            REFERENCES tasks(id)
            ON DELETE CASCADE,
    CONSTRAINT fk_attributes
      FOREIGN KEY(attribute_id)
          REFERENCES attributes(id)
          ON DELETE CASCADE,
    CONSTRAINT fk_tasks
      FOREIGN KEY(task_id)
          REFERENCES tasks(id)
          ON DELETE CASCADE
);

CREATE TABLE IF NOT EXISTS attr_validations (
    id SERIAL PRIMARY KEY,
    min smallint,
    max smallint,
    regex VARCHAR(255),
    attribute_value_id INT,
    created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT fk_attribute_values
        FOREIGN KEY(attribute_value_id)
            REFERENCES attribute_values(id)
            ON DELETE CASCADE
);

SELECT diesel_manage_updated_at('attribute_values');
SELECT diesel_manage_updated_at('attr_validations');
