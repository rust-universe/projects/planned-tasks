DROP TABLE IF EXISTS logging.t_history;
DROP SCHEMA IF EXISTS logging;
DROP FUNCTION IF EXISTS audit_log(_tbl regclass);
DROP FUNCTION IF EXISTS change_trigger();