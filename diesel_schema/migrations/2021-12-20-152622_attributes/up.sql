CREATE TYPE attribute_value_data_type AS ENUM ('text','rich_text','number','color','date',
    'boolean', 'binary','currency','table','list','range');

create table IF NOT EXISTS attributes (
    id SERIAL PRIMARY KEY,
    display_text VARCHAR(255) NOT NULL,
    description TEXT,
    data_type attribute_value_data_type NOT NULL,
    is_template BOOLEAN NOT NULL DEFAULT FALSE,
    created_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMPTZ NOT NULL DEFAULT CURRENT_TIMESTAMP
);

SELECT diesel_manage_updated_at('attributes');
