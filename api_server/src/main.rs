// Copyright 2020 Sudhir A. Dhumal <sudhirdhumal289@gmail.com>
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
// http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use actix_web::{web, App, HttpServer};
use api_server::{health_check, planner, utilities};
use log::info;

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    // Setup logger
    let _ = utilities::logger::setup_logger();

    info!("Starting api-server.");

    let server_config = utilities::server_config::ServerConfig::new(10);

    let server = HttpServer::new(move || {
        App::new()
            .app_data(server_config.app_state.clone())
            .configure(planner::register_routes)
            .configure(health_check::register_routes)
            .app_data(
                web::JsonConfig::default()
                    .error_handler(api_server::utilities::actix_config::json_error_handler),
            )
            .app_data(
                web::PathConfig::default()
                    .error_handler(api_server::utilities::actix_config::path_error_handler),
            )
    });

    if server_config.is_https {
        server
            .bind_openssl(
                format!("127.0.0.1:{}", server_config.port),
                server_config.ssl_builder.unwrap(),
            )?
            .run()
            .await
    } else {
        server
            .bind(format!("127.0.0.1:{}", server_config.port))?
            .run()
            .await
    }
}
