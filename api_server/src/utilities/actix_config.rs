use crate::api_response::ApiResponse;
use actix_web::error::PathError;
use log::error;

#[derive(serde::Serialize)]
pub struct JSONError;

pub fn json_error_handler(
    err: actix_web::error::JsonPayloadError,
    _req: &actix_web::HttpRequest,
) -> actix_web::error::Error {
    use actix_web::error::JsonPayloadError;

    let detail = if cfg!(debug_assertions) {
        Some(err.to_string())
    } else {
        None
    };

    error!(
        "Error occurred while processing JSON request: {}",
        err.to_string()
    );

    let resp = match &err {
        JsonPayloadError::ContentType => actix_web::HttpResponse::UnsupportedMediaType().json(
            ApiResponse::<JSONError>::from_error_and_details(
                "Unsupported media-type, make sure you are passing valid content type.".to_string(),
                String::from("UnsupportedMediaType"),
                detail,
            ),
        ),
        JsonPayloadError::Deserialize(json_err) if json_err.is_data() => {
            actix_web::HttpResponse::UnprocessableEntity().json(
                ApiResponse::<JSONError>::from_error_and_details(
                    "Unable to parse the request. Make sure you are passing valid request body."
                        .to_string(),
                    String::from("UnprocessableEntity"),
                    detail,
                ),
            )
        }
        _ => actix_web::HttpResponse::BadRequest().json(
            ApiResponse::<JSONError>::from_error_and_details(
                "Bad request, Make sure you are passing valid request body.".to_string(),
                String::from("BadRequest"),
                detail,
            ),
        ),
    };

    actix_web::error::InternalError::from_response(err, resp).into()
}

pub fn path_error_handler(
    err: PathError,
    _req: &actix_web::HttpRequest,
) -> actix_web::error::Error {
    let detail = err.to_string();

    error!(
        "Error occurred while extracting Path parameter from request URL: {}",
        detail
    );

    let resp = actix_web::HttpResponse::BadRequest().json(
        ApiResponse::<JSONError>::from_error_and_details(
            "Make sure you are using valid path parameter(s) while calling an API.".to_string(),
            String::from("BadRequest"),
            if cfg!(debug_assertions) {
                Some(detail)
            } else {
                None
            },
        ),
    );

    actix_web::error::InternalError::from_response(err, resp).into()
}

#[cfg(test)]
mod test {
    use actix_web::test as actix_test;
    use serde::{Deserialize, Serialize};

    #[test]
    fn when_content_type_is_wrong() {
        let err = actix_web::error::JsonPayloadError::ContentType;
        let req = actix_test::TestRequest::default().to_http_request();

        let error_resp = super::json_error_handler(err, &req);

        let resp_body = error_resp.error_response();
        let dbg = format!("{:?}", resp_body.body());
        assert!(dbg.contains("UnsupportedMediaType"));
        assert!(
            dbg.contains("Unsupported media-type, make sure you are passing valid content type.")
        );
    }

    #[test]
    fn when_bad_reqeust_is_received() {
        let err = actix_web::error::JsonPayloadError::Deserialize(
            serde_json::from_str::<i32>("bad payload").unwrap_err(),
        );
        let req = actix_test::TestRequest::default().to_http_request();

        let error_resp = super::json_error_handler(err, &req);

        let resp_body = error_resp.error_response();
        let dbg = format!("{:?}", resp_body.body());
        assert!(dbg.contains("BadRequest"));
        assert!(dbg.contains("Bad request, Make sure you are passing valid request body."));
    }

    #[test]
    fn when_deserialize_operation_is_failed() {
        #[derive(Serialize, Deserialize, Debug)]
        pub struct A {
            pub id: i32,
        }

        let err = actix_web::error::JsonPayloadError::Deserialize(
            serde_json::from_str::<A>("{\"id\": null}").unwrap_err(),
        );
        let req = actix_test::TestRequest::default().to_http_request();

        let error_resp = super::json_error_handler(err, &req);

        let resp_body = error_resp.error_response();
        let dbg = format!("{:?}", resp_body.body());
        assert!(dbg.contains("UnprocessableEntity"));
        assert!(dbg.contains(
            "Unable to parse the request. Make sure you are passing valid request body."
        ));
    }

    #[actix_web::get("/{attribute_id}")]
    async fn test_controller_handler(path: actix_web::web::Path<i32>) -> impl actix_web::Responder {
        let attribute_id = path.into_inner();

        format!("Attribute ID {}", attribute_id)
    }

    #[actix_rt::test]
    async fn when_invalid_path_value_passed() {
        let app = actix_web::test::init_service(
            actix_web::App::new()
                .service(test_controller_handler)
                .app_data(
                    actix_web::web::PathConfig::default()
                        .error_handler(crate::utilities::actix_config::path_error_handler),
                ),
        )
        .await;

        // Create test request
        let req = actix_web::test::TestRequest::with_uri(
            format!("/{}", chrono::Utc::now().timestamp_millis()).as_str(),
        )
        .to_request();

        // Hit API endpoint with test request.
        let resp = actix_web::test::call_service(&app, req).await;
        assert!(resp.status().is_client_error());

        let failure_resp: crate::api_response::ApiResponse<String> =
            actix_web::test::read_body_json(resp).await;
        let failure_resp = failure_resp.errors.unwrap();
        let first_error = failure_resp.first().unwrap();

        assert_eq!(
            first_error.message,
            "Make sure you are using valid path parameter(s) while calling an API.".to_string()
        );
        assert_eq!(first_error.code, "BadRequest");

        // Check if API returns response if valid number passed
        let req = actix_web::test::TestRequest::with_uri("/123").to_request();
        let resp = actix_web::test::call_service(&app, req).await;
        assert!(resp.status().is_success());
    }
}
