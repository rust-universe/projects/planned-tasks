use super::pg_util::PgUtils;

use std::env;

use crate::application_state::ApplicationState;

use actix_web::web;
use chrono::Utc;
use log::debug;
use openssl::ssl::{SslAcceptor, SslAcceptorBuilder, SslFiletype, SslMethod};

pub struct ServerConfig {
    pub port: u16,
    pub is_https: bool,
    private_key_file_path: Option<String>,
    certificate_file_path: Option<String>,
    pub ssl_builder: Option<SslAcceptorBuilder>,
    pub app_state: web::Data<ApplicationState>,
}

impl ServerConfig {
    pub fn new(conn_pool_site: u32) -> Self {
        let mut server_config = ServerConfig {
            port: ServerConfig::get_port_number(false),
            is_https: false,
            private_key_file_path: None,
            certificate_file_path: None,
            ssl_builder: None,
            app_state: ServerConfig::get_app_state(conn_pool_site),
        };

        match env::var("HTTPS_PRIVATE_KEY") {
            Ok(private_key) => match env::var("HTTPS_CERT") {
                Ok(cert) => {
                    server_config.private_key_file_path = Some(private_key.clone());
                    server_config.certificate_file_path = Some(cert.clone());
                    server_config.port = ServerConfig::get_port_number(true);
                    server_config.is_https = true;
                    server_config.ssl_builder = Some(ServerConfig::get_ssl_acceptor_builder(
                        private_key.as_str(),
                        cert.as_str(),
                    ));
                }
                Err(_) => {
                    debug!(
                        "Certificate file path not provided. \
                        Server will be starting on HTTP rather than HTTPS"
                    );
                }
            },
            Err(_) => {
                debug!(
                    "Private key file path not provided. \
                Server will be starting on HTTP rather than HTTPS"
                );
            }
        };

        server_config
    }

    fn get_app_state(conn_pool_site: u32) -> web::Data<ApplicationState> {
        web::Data::new(ApplicationState {
            pool: PgUtils::get_instance().get_connection_pool(conn_pool_site),
            started_at: Utc::now(),
        })
    }

    fn get_port_number(is_https: bool) -> u16 {
        match env::var("RUST_PORT") {
            Ok(port) => port.parse().unwrap(),
            Err(_) => {
                if is_https {
                    8443
                } else {
                    8080
                }
            }
        }
    }

    fn get_ssl_acceptor_builder(
        private_key_file_path: &str,
        certificate_file_path: &str,
    ) -> SslAcceptorBuilder {
        let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
        builder
            .set_private_key_file(private_key_file_path, SslFiletype::PEM)
            .unwrap();
        builder
            .set_certificate_chain_file(certificate_file_path)
            .unwrap();

        builder
    }
}

impl Default for ServerConfig {
    fn default() -> Self {
        Self::new(10)
    }
}

#[cfg(test)]
mod tests {
    use super::ServerConfig;
    use openssl::asn1::Asn1Time;
    use openssl::hash::MessageDigest;
    use openssl::nid::Nid;
    use openssl::pkey::{PKey, Private};
    use openssl::rsa::Rsa;
    use openssl::x509::{X509Name, X509};
    use std::env;
    use std::io::Write;
    use tempfile::NamedTempFile;

    fn pkey() -> PKey<Private> {
        let rsa = Rsa::generate(4096).unwrap();
        PKey::from_rsa(rsa).unwrap()
    }

    #[test]
    fn validate_config_for_cert_key_and_port() {
        let pkey = pkey();
        let mut name = X509Name::builder().unwrap();
        name.append_entry_by_nid(Nid::COMMONNAME, "foobar.com")
            .unwrap();
        let name = name.build();
        let mut builder = X509::builder().unwrap();
        builder.set_version(2).unwrap();
        builder.set_subject_name(&name).unwrap();
        builder.set_issuer_name(&name).unwrap();
        builder
            .set_not_before(&Asn1Time::days_from_now(0).unwrap())
            .unwrap();
        builder
            .set_not_after(&Asn1Time::days_from_now(365).unwrap())
            .unwrap();
        builder.set_pubkey(&pkey).unwrap();
        builder.sign(&pkey, MessageDigest::sha256()).unwrap();
        let x509 = builder.build();

        let private_key = pkey.private_key_to_pem_pkcs8().unwrap();
        let cert = x509.to_pem().unwrap();

        let mut private_key_file = NamedTempFile::new().unwrap();
        write!(
            private_key_file,
            "{}",
            String::from_utf8(private_key).unwrap()
        )
        .unwrap();

        let mut public_cert_file = NamedTempFile::new().unwrap();
        write!(public_cert_file, "{}", String::from_utf8(cert).unwrap()).unwrap();

        env::set_var(
            "HTTPS_PRIVATE_KEY",
            private_key_file.path().to_str().unwrap(),
        );
        env::set_var("HTTPS_CERT", public_cert_file.path().to_str().unwrap());
        println!();

        let server_config = ServerConfig::default();
        assert_eq!(server_config.port, 8443);
        assert_eq!(server_config.is_https, true);

        // Remove cert env var and then check for config.
        env::remove_var("HTTPS_CERT");
        let server_config = ServerConfig::default();
        assert_eq!(server_config.port, 8080);
        assert_eq!(server_config.is_https, false);

        // Remove key env var and then check for config.
        env::remove_var("HTTPS_PRIVATE_KEY");
        let server_config = ServerConfig::default();
        assert_eq!(server_config.port, 8080);
        assert_eq!(server_config.is_https, false);

        // Set port and check for config.
        env::set_var("RUST_PORT", "8585");
        let server_config = ServerConfig::default();
        assert_eq!(server_config.port, 8585);
        assert_eq!(server_config.is_https, false);
    }
}
