use fern::colors::{Color, ColoredLevelConfig};
use fern::Dispatch;
use std::io;

pub fn setup_logger() -> Result<(), fern::InitError> {
    fern::Dispatch::new()
        .chain(get_logger_config_for_stdout())
        .chain(get_logger_config_for_file())
        .apply()?;

    Ok(())
}

fn get_logger_config_for_stdout() -> Dispatch {
    let colors_line = ColoredLevelConfig::new()
        .error(Color::BrightRed)
        .warn(Color::BrightYellow)
        .info(Color::BrightGreen)
        .debug(Color::BrightCyan)
        // depending on the terminals color scheme, this is the same as the background color
        .trace(Color::BrightBlack);

    fern::Dispatch::new()
        .format(move |out, message, record| {
            out.finish(format_args!(
                "{color_line}[{date}][{target}][{level}{color_line}] {message}\x1B[0m",
                color_line = format_args!(
                    "\x1B[{}m",
                    colors_line.get_color(&record.level()).to_fg_str()
                ),
                date = chrono::Local::now().format("%Y-%m-%d %H:%M:%S"),
                target = record.target(),
                level = colors_line.color(record.level()),
                message = message
            ))
        })
        .level(log::LevelFilter::Debug)
        .chain(io::stdout())
}

fn get_logger_config_for_file() -> Dispatch {
    fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "[{}][{}][{}] {}",
                chrono::Local::now().format("%Y-%m-%d %H:%M:%S"),
                record.target(),
                record.level(),
                message
            ))
        })
        .level(log::LevelFilter::Info)
        .chain(fern::log_file("output.log").unwrap())
}

#[cfg(test)]
mod tests {
    use log::{info, trace};
    use std::fs::File;
    use std::io::Read;

    #[test]
    fn test_logger_config() {
        super::setup_logger().unwrap();

        trace!("SHOULD NOT DISPLAY");
        info!("Visible message from logger config test case");

        // Flush messages so that we can open fiele and verify the content.
        log::logger().flush();

        let result = {
            let mut log_read = File::open("output.log").unwrap();
            let mut buf = String::new();
            log_read.read_to_string(&mut buf).unwrap();
            buf
        };

        assert!(
            !result.contains("SHOULD NOT DISPLAY"),
            "expected result not including \"SHOULD_NOT_DISPLAY\", found:\n```\n{}\n```\n",
            result
        );
        assert!(
            result.contains("[INFO] Visible message from logger config test case"),
            "expected result including \"[INFO] Visible message from logger config test case\", found:\n```\n{}\n```\n",
            result
        );
    }
}
