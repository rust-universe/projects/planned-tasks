use diesel::r2d2::{ConnectionManager, Pool};
use diesel::PgConnection;
use std::env;

pub struct PgUtils {
    user: String,
    password: String,
    host: String,
    port: String,
    db_name: String,
}

impl PgUtils {
    /// Read database specific environment variables and create an instance.
    ///
    /// # Panics
    /// Instance creation will panic if database specific variables are not set.
    pub fn get_instance() -> PgUtils {
        let user = env::var("DB_USER").expect("DB_USER Environment variable not set");
        let mut password =
            env::var("DB_PASSWORD").expect("DB_PASSWORD Environment variable not set");
        let host = env::var("DB_HOST").expect("DB_HOST Environment variable not set");
        let db_name = env::var("DB_NAME").expect("DB_NAME Environment variable not set");

        if password.starts_with('\'') && password.ends_with('\'') {
            password = (&password[1..password.len() - 1]).to_string();
        }
        password = urlencoding::encode(&password).into_owned();

        // postgres port-number environment variable is optional,
        // If not provided then diesel will try to connect to postgres on its default port number.
        let port = match env::var("DB_HOST_PORT") {
            Ok(port) => format!(":{}", port),
            Err(_) => "".to_string(),
        };

        PgUtils {
            user,
            password,
            host,
            port,
            db_name,
        }
    }

    fn get_database_url(&self) -> String {
        format!(
            "postgresql://{}:{}@{}{}/{}",
            self.user, self.password, self.host, self.port, self.db_name
        )
    }

    /// This function will create a thread safe PgConnection pool instance.
    /// It uses the database URL generated above to build the connection manager and then
    /// connection pool out of connection manager.
    ///
    /// # How to use:
    /// Set DB specific environment variables first and then invoke this method
    /// to get the connection pool and the register it as app-data in actix server,
    /// so that it can used by the route handlers.
    ///
    /// # Panics
    /// If any of the required database specific environment variables are not set
    /// then application will panic.
    pub fn get_connection_pool(
        &self,
        conn_pool_site: u32,
    ) -> Pool<ConnectionManager<PgConnection>> {
        let database_url = self.get_database_url();
        let connection_manager = ConnectionManager::<PgConnection>::new(database_url);

        Pool::builder()
            .max_size(conn_pool_site)
            .build(connection_manager)
            .expect("Failed to create DB connection pool")
    }
}

#[cfg(test)]
mod tests {
    use super::PgUtils;
    use std::env;

    #[test]
    fn when_env_set() {
        let user = env::var("DB_USER").expect("DB_USER Environment variable not set");
        let mut password =
            env::var("DB_PASSWORD").expect("DB_PASSWORD Environment variable not set");
        let host = env::var("DB_HOST").expect("DB_HOST Environment variable not set");
        let db_name = env::var("DB_NAME").expect("DB_NAME Environment variable not set");
        let port = match env::var("DB_HOST_PORT") {
            Ok(port) => format!(":{}", port),
            Err(_) => "".to_string(),
        };
        if password.starts_with('\'') && password.ends_with('\'') {
            password = (&password[1..password.len() - 1]).to_string();
        }
        password = urlencoding::encode(&password).into_owned();

        let pg_utils = PgUtils::get_instance();
        assert_eq!(
            pg_utils.get_database_url(),
            format!(
                "postgresql://{}:{}@{}{}/{}",
                user, password, host, port, db_name
            )
        );
    }
}
