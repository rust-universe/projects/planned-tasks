use actix_web::{get, web, HttpResponse};
use serde::{Deserialize, Serialize};

use crate::application_state::ApplicationState;

pub fn register_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(health_check);
}

#[derive(Serialize, Deserialize)]
struct HealthStatus {
    message: String,
}

#[get("/health_check")]
async fn health_check(
    app_state: web::Data<ApplicationState>,
) -> actix_web::Result<HttpResponse, actix_web::Error> {
    let health_status = HealthStatus {
        message: format!("Server is up and running since {}", app_state.started_at),
    };

    Ok(HttpResponse::Ok().json(health_status))
}

#[cfg(test)]
mod tests {
    use actix_web::{test, web, App};
    use chrono::Utc;

    use crate::{application_state::ApplicationState, utilities::pg_util::PgUtils};

    #[actix_rt::test]
    async fn test_health_check() {
        let app_state = web::Data::new(ApplicationState {
            pool: PgUtils::get_instance().get_connection_pool(1),
            started_at: Utc::now(),
        });
        let app = test::init_service(
            App::new()
                .app_data(app_state)
                .configure(super::register_routes),
        )
        .await;

        // Create request object
        let req = test::TestRequest::get().uri("/health_check").to_request();

        // Call application
        let resp = test::call_service(&app, req).await;
        assert!(resp.status().is_success());
    }
}
