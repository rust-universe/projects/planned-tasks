use chrono::{DateTime, Utc};
use diesel::r2d2::{ConnectionManager, Pool};
use diesel::PgConnection;

pub struct ApplicationState {
    pub pool: Pool<ConnectionManager<PgConnection>>,
    pub started_at: DateTime<Utc>,
}
