use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub struct ApiError {
    pub message: String,
    pub details: Option<String>,
    pub code: String,
}

#[derive(Serialize, Deserialize, Debug)]
pub struct ApiResponse<T: Serialize> {
    pub response: Option<T>,
    pub errors: Option<Vec<ApiError>>,
}

impl<T: Serialize> ApiResponse<T> {
    pub fn from_error(message: String, code: String) -> ApiResponse<T> {
        ApiResponse {
            response: None,
            errors: Some(vec![ApiError {
                code,
                message,
                details: None,
            }]),
        }
    }

    pub fn from_error_and_details(
        message: String,
        code: String,
        details: Option<String>,
    ) -> ApiResponse<T> {
        ApiResponse {
            response: None,
            errors: Some(vec![ApiError {
                code,
                message,
                details,
            }]),
        }
    }

    pub fn from_response(response: T) -> ApiResponse<T> {
        ApiResponse {
            response: Some(response),
            errors: None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::ApiError;
    use super::ApiResponse;
    use serde::Serialize;

    #[test]
    fn error_message_and_code() {
        let error_resp =
            ApiResponse::<String>::from_error("Error Message".to_string(), "SomeError".to_string());

        let errors_opt = error_resp.errors.unwrap();
        let generated_error_resp = errors_opt.get(0).unwrap();
        assert_eq!(generated_error_resp.message, "Error Message");
        assert_eq!(generated_error_resp.code, "SomeError");
    }

    #[test]
    fn error_message_and_code_and_details() {
        let error_resp = ApiResponse::<String>::from_error_and_details(
            "Error Message".to_string(),
            "SomeError".to_string(),
            Some("Some details about error".to_string()),
        );

        let errors_opt = error_resp.errors.unwrap();
        let generated_error_resp = errors_opt.get(0).unwrap();
        assert_eq!(generated_error_resp.message, "Error Message");
        assert_eq!(generated_error_resp.code, "SomeError");
        assert_eq!(
            generated_error_resp.details,
            Some("Some details about error".to_string())
        );
    }

    #[test]
    fn check_for_valid_resp() {
        #[derive(Serialize)]
        struct A {
            msg: String,
        }

        let the_obj = A {
            msg: "test".to_string(),
        };
        let success_resp = ApiResponse::<A>::from_response(the_obj);

        let response_instance = success_resp.response.unwrap();
        assert_eq!(response_instance.msg, "test");
        assert_eq!(success_resp.errors, None);
    }

    #[test]
    fn response_by_passing_api_error() {
        let api_error = ApiError {
            message: "this is message".to_string(),
            details: None,
            code: "ErrorCode".to_string(),
        };

        let api_resp = ApiResponse::<String> {
            errors: Some(vec![api_error]),
            response: None,
        };

        let errors_resp = api_resp.errors.unwrap();
        assert_eq!(errors_resp.len(), 1);
        assert_eq!(errors_resp.get(0).unwrap().message, "this is message");
    }
}
