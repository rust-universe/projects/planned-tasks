mod attribute;
mod project;
mod task;

use actix_web::web;

pub fn register_routes(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/api")
            .configure(attribute::configure)
            .configure(project::configure)
            .configure(task::configure),
    );
}
