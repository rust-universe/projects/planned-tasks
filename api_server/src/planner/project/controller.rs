use actix_web::{delete, get, post, put, web, HttpResponse, Responder};

#[get("/{project_id}")]
pub async fn fetch(_path: web::Path<i32>) -> impl Responder {
    HttpResponse::Ok().body("get project details")
}

#[get("/")]
pub async fn fetch_all() -> impl Responder {
    HttpResponse::Ok().body("get all project details")
}

#[post("/")]
pub async fn create() -> impl Responder {
    HttpResponse::Ok().body("Created")
}

#[put("/")]
pub async fn update() -> impl Responder {
    HttpResponse::Ok().body("Updated")
}

#[delete("/{project_id}")]
pub async fn delete(_path: web::Path<i32>) -> impl Responder {
    HttpResponse::Ok().body("Deleted")
}

#[post("/upsert")]
pub async fn upsert() -> impl Responder {
    HttpResponse::Ok().body("Upsert")
}
