mod controller;

use actix_web::web;

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/project")
            .service(controller::fetch)
            .service(controller::create)
            .service(controller::update)
            .service(controller::upsert),
    );
}
