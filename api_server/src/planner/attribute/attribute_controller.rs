use actix_web::{delete, get, post, put, web, HttpResponse};
use log::{error, info, warn};

use crate::api_response::ApiResponse;
use crate::application_state::ApplicationState;

use mod_common::PaginationQuery;
use schema::attribute::Attribute;
use schema::attribute::NewAttribute;
use schema::EntityError;

use super::attribute_model::AttributeModel;
use super::attribute_model::NewAttributeModel;

#[post("/")]
pub(crate) async fn create(
    app_state: web::Data<ApplicationState>,
    req_attribute_model: web::Json<NewAttributeModel>,
) -> actix_web::Result<HttpResponse, actix_web::Error> {
    info!("Received request to create an attribute");

    let req_attribute_model = req_attribute_model.into_inner();
    let attribute_model = NewAttribute {
        display_text: req_attribute_model.display_text,
        description: req_attribute_model.description,
        data_type: req_attribute_model.data_type,
    };

    // TODO - validation, sanitization of data

    let mut conn = app_state
        .pool
        .get()
        .expect("Failed to get the connection from connection pool");

    let resp = web::block(move || Attribute::create(&mut conn, attribute_model, false)).await?;

    Ok(match resp {
        Ok(attribute_saved) => {
            info!("Attribute saved in database with ID {}", attribute_saved.id);
            HttpResponse::Ok().json(ApiResponse::from_response(AttributeModel::from(
                attribute_saved,
            )))
        }
        Err(schema_error) => match *schema_error {
            EntityError::RecordAlreadyExists => {
                let msg = "Attribute with same display-text already exists.";
                warn!("{}", msg);
                HttpResponse::BadRequest().json(ApiResponse::<ApiResponse<Attribute>>::from_error(
                    msg.to_string(),
                    schema_error.to_string(),
                ))
            }
            _ => {
                error!("Failed to save the attribute details.");
                HttpResponse::InternalServerError().json(
                    ApiResponse::<ApiResponse<Attribute>>::from_error(
                        "Failed to create an attribute.".to_string(),
                        schema_error.to_string(),
                    ),
                )
            }
        },
    })
}

#[get("")]
pub(crate) async fn fetch_all(
    pagination_query: web::Query<PaginationQuery>,
    app_state: web::Data<ApplicationState>,
) -> actix_web::Result<HttpResponse, actix_web::Error> {
    let mut conn = app_state
        .pool
        .get()
        .expect("Failed to get the connection from connection pool");

    let pagination_query = pagination_query.into_inner();

    let resp = web::block(move || Attribute::get_all(&mut conn, pagination_query)).await?;

    Ok(match resp {
        Ok(result) => HttpResponse::Ok().json(ApiResponse::from_response(result)),
        Err(schema_error) => match *schema_error {
            EntityError::DatabaseError => {
                error!("Failed to fetch the attribute records");
                HttpResponse::InternalServerError().json(
                    ApiResponse::<ApiResponse<Attribute>>::from_error(
                        "Failed to fetch the attribute records from database.".to_string(),
                        schema_error.to_string(),
                    ),
                )
            }
            _ => HttpResponse::InternalServerError().json(
                ApiResponse::<ApiResponse<Attribute>>::from_error(
                    "Something went wrong, please try again or try after sometime".to_string(),
                    schema_error.to_string(),
                ),
            ),
        },
    })
}

#[put("/")]
pub(crate) async fn update() -> actix_web::Result<HttpResponse, actix_web::Error> {
    Ok(HttpResponse::Ok().json(ApiResponse::from_response("update")))
}

#[get("/{attribute_id}")]
pub(crate) async fn fetch(
    app_state: web::Data<ApplicationState>,
    path: web::Path<i32>,
) -> actix_web::Result<HttpResponse, actix_web::Error> {
    let attribute_id = path.into_inner();
    info!("Finding an attribute with ID {}", attribute_id);

    let mut conn = app_state
        .pool
        .get()
        .expect("Failed to get the connection from connection pool");
    let resp = web::block(move || Attribute::find_by_id(&mut conn, attribute_id)).await?;

    Ok(match resp {
        Ok(record_fount) => {
            info!("Attribute found for attribute ID: {}", attribute_id);
            HttpResponse::Ok().json(ApiResponse::from_response(AttributeModel::from(
                record_fount,
            )))
        }
        Err(schema_error) => match *schema_error {
            EntityError::RecordNotFound => {
                let msg = format!(
                    "Unable to find an attribute for provided attribute ID ({})",
                    attribute_id
                );
                error!("{}", msg);
                HttpResponse::BadRequest().json(ApiResponse::<ApiResponse<Attribute>>::from_error(
                    msg,
                    schema_error.to_string(),
                ))
            }
            _ => {
                error!(
                    "Something went wrong while finding an attribute with ID ({})",
                    attribute_id
                );
                HttpResponse::InternalServerError().json(
                    ApiResponse::<ApiResponse<Attribute>>::from_error(
                        "Something went wrong, please try again or try after sometime".to_string(),
                        schema_error.to_string(),
                    ),
                )
            }
        },
    })
}

#[delete("/{attribute_id}")]
pub(crate) async fn delete(
    _path: web::Path<i32>,
) -> actix_web::Result<HttpResponse, actix_web::Error> {
    Ok(HttpResponse::Ok().json(ApiResponse::from_response("delete")))
}

#[post("/upsert")]
pub(crate) async fn upsert() -> actix_web::Result<HttpResponse, actix_web::Error> {
    Ok(HttpResponse::Ok().json(ApiResponse::from_response("upsert")))
}

#[cfg(test)]
mod tests {
    use actix_web::{test, App};

    use crate::api_response::ApiResponse;
    use crate::planner::attribute::attribute_model::AttributeModel;
    use crate::utilities;

    use mod_common::PaginationResult;

    use chrono::Utc;
    use diesel::r2d2::{ConnectionManager, PooledConnection};
    use diesel::PgConnection;
    use schema::attribute::{Attribute, AttributeValueDataType, NewAttribute};

    fn create_mock_attr(mut conn: PooledConnection<ConnectionManager<PgConnection>>) -> Attribute {
        // Create an attribute before fetching an attribute.
        let display_text = format!("display text - {}", Utc::now().timestamp_nanos());
        let description = Some(format!("description - {}", Utc::now().timestamp_nanos()));
        let attribute_model = NewAttribute {
            display_text,
            description,
            data_type: AttributeValueDataType::Text,
        };

        Attribute::create(&mut conn, attribute_model, false).unwrap()
    }

    #[actix_rt::test]
    async fn test_fetch_attribute_details() {
        let server_config = utilities::server_config::ServerConfig::new(1);
        let app = test::init_service(
            App::new()
                .app_data(server_config.app_state.clone())
                .service(super::fetch),
        )
        .await;

        let attr_created = create_mock_attr(server_config.app_state.pool.get().unwrap());

        // Create test request
        let req =
            test::TestRequest::with_uri(format!("/{}", attr_created.id).as_str()).to_request();

        // Hit API endpoint with test request.
        let resp = test::call_service(&app, req).await;
        assert!(resp.status().is_success());

        let attr_resp: ApiResponse<AttributeModel> = test::read_body_json(resp).await;
        let attr = attr_resp.response.unwrap();

        assert_eq!(attr_created.id, attr.id);
        assert_eq!(attr_created.description.unwrap(), attr.description.unwrap());
        assert_eq!(attr_created.display_text, attr.display_text);
        assert_eq!(attr_created.data_type, attr.data_type);
        assert!(!attr.is_template);
    }

    #[actix_rt::test]
    async fn test_fetch_non_existing_attribute_details() {
        let server_config = utilities::server_config::ServerConfig::new(1);
        let app = test::init_service(
            App::new()
                .app_data(server_config.app_state.clone())
                .service(super::fetch),
        )
        .await;

        // Create and delete attribute directly - by bypassing the controller method
        let display_text = format!("display text - {}", Utc::now().timestamp_nanos());
        let description = Some(format!("description - {}", Utc::now().timestamp_nanos()));

        let attribute_model = super::NewAttribute {
            description: description.clone(),
            display_text: display_text.clone(),
            data_type: AttributeValueDataType::Text,
        };
        let attribute_created = Attribute::create(
            &mut server_config.app_state.pool.get().unwrap(),
            attribute_model,
            false,
        )
        .unwrap();

        let attribute_id = attribute_created.id;
        let attr_deletion_result = Attribute::delete_record(
            &mut server_config.app_state.pool.get().unwrap(),
            attribute_created.id,
        )
        .unwrap();
        assert_eq!(attr_deletion_result, true);

        // Create test request
        let req = test::TestRequest::with_uri(format!("/{}", attribute_id).as_str()).to_request();

        // Hit API endpoint with test request.
        let resp = test::call_service(&app, req).await;
        assert!(resp.status().is_client_error());

        let attr_resp: ApiResponse<AttributeModel> = test::read_body_json(resp).await;
        let attr_failure_resp = attr_resp.errors.unwrap();

        assert_eq!(attr_failure_resp.len(), 1);

        let first_error = attr_failure_resp.first().unwrap();

        let msg = format!(
            "Unable to find an attribute for provided attribute ID ({})",
            attribute_id
        );
        assert_eq!(first_error.message, msg);
        assert_eq!(first_error.code, "RecordNotFound");
    }

    #[actix_rt::test]
    async fn test_create_attribute() {
        let server_config = utilities::server_config::ServerConfig::new(1);
        let mut app = test::init_service(
            App::new()
                .app_data(server_config.app_state.clone())
                .service(super::create),
        )
        .await;

        // Payload
        let display_text = format!("display text - {}", Utc::now().timestamp_nanos());
        let description = Some(format!("description - {}", Utc::now().timestamp_nanos()));
        let attribute_model = super::NewAttributeModel {
            description: description.clone(),
            display_text: display_text.clone(),
            data_type: AttributeValueDataType::Text,
        };
        let payload = serde_json::to_string_pretty(&attribute_model).unwrap();

        // Create test request and hit the endpoint
        let api_response = test::TestRequest::post()
            .uri("/")
            .insert_header(actix_web::http::header::ContentType::json())
            .set_payload(payload)
            .send_request(&mut app)
            .await;

        assert!(api_response.status().is_success());

        let attr_resp: ApiResponse<AttributeModel> = test::read_body_json(api_response).await;
        let attr_created = attr_resp.response.unwrap();

        assert!(attr_created.id.is_positive());
        assert_eq!(attr_created.description.unwrap(), description.unwrap());
        assert_eq!(attr_created.display_text, display_text);
        assert_eq!(attr_created.data_type, AttributeValueDataType::Text);
        assert!(!attr_created.is_template);
    }

    #[actix_rt::test]
    async fn test_create_attribute_for_same_display_text() {
        let server_config = utilities::server_config::ServerConfig::new(1);
        let mut app = test::init_service(
            App::new()
                .app_data(server_config.app_state.clone())
                .service(super::create),
        )
        .await;

        // Create attribute directly - by bypassing the controller method
        let display_text = format!("display text - {}", Utc::now().timestamp_nanos());
        let description = Some(format!("description - {}", Utc::now().timestamp_nanos()));

        let attribute_model = super::NewAttribute {
            description: description.clone(),
            display_text: display_text.clone(),
            data_type: AttributeValueDataType::Text,
        };
        let attribute_created = Attribute::create(
            &mut server_config.app_state.pool.get().unwrap(),
            attribute_model,
            false,
        )
        .unwrap();
        assert_eq!(attribute_created.display_text, display_text);

        // Hit controller method with the exact same payload
        let attribute_model = super::NewAttributeModel {
            description: description.clone(),
            display_text: display_text.clone(),
            data_type: AttributeValueDataType::Text,
        };
        let payload = serde_json::to_string_pretty(&attribute_model).unwrap();

        // Create test request and hit the endpoint
        let api_response = test::TestRequest::post()
            .uri("/")
            .insert_header(actix_web::http::header::ContentType::json())
            .set_payload(payload)
            .send_request(&mut app)
            .await;

        assert!(api_response.status().is_client_error());

        let attr_resp: ApiResponse<AttributeModel> = test::read_body_json(api_response).await;
        let attr_created = attr_resp.errors.unwrap();

        assert_eq!(attr_created.len(), 1);

        let first_error = attr_created.first().unwrap();
        assert_eq!(first_error.code, "RecordAlreadyExists");
        assert_eq!(
            first_error.message,
            "Attribute with same display-text already exists."
        );
    }

    #[actix_rt::test]
    async fn test_fetch_all_attributes() {
        use actix_web::web;

        let server_config = utilities::server_config::ServerConfig::new(1);
        let mut app = test::init_service(
            App::new()
                .app_data(server_config.app_state.clone())
                .service(web::scope("/attribute").service(super::fetch_all)),
        )
        .await;

        // Create temporary attribute records
        for _ in 1..20 {
            create_mock_attr(server_config.app_state.pool.get().unwrap());
        }

        // Create test request and hit the endpoint
        let api_response = test::TestRequest::get()
            .uri("/attribute")
            .insert_header(actix_web::http::header::ContentType::json())
            .send_request(&mut app)
            .await;

        assert!(api_response.status().is_success());

        let attr_resp: ApiResponse<PaginationResult<Attribute>> =
            test::read_body_json(api_response).await;
        let attr_created = attr_resp.response.unwrap();

        assert_eq!(attr_created.query.page, 1);
        assert_eq!(attr_created.query.per_page, 10);
        assert_eq!(attr_created.items.len(), 10);
    }

    #[actix_rt::test]
    async fn test_fetch_all_attributes_with_page_and_page_size() {
        use actix_web::web;

        let server_config = utilities::server_config::ServerConfig::new(1);
        let mut app = test::init_service(
            App::new()
                .app_data(server_config.app_state.clone())
                .service(web::scope("/attribute").service(super::fetch_all)),
        )
        .await;

        // Create temporary attribute records
        for _ in 1..20 {
            create_mock_attr(server_config.app_state.pool.get().unwrap());
        }

        // Create test request and hit the endpoint
        let api_response = test::TestRequest::get()
            .uri("/attribute?page=2&perPage=5")
            .insert_header(actix_web::http::header::ContentType::json())
            .send_request(&mut app)
            .await;

        assert!(api_response.status().is_success());

        let attr_resp: ApiResponse<PaginationResult<Attribute>> =
            test::read_body_json(api_response).await;
        let attr_created = attr_resp.response.unwrap();

        assert_eq!(attr_created.query.page, 2);
        assert_eq!(attr_created.query.per_page, 5);
        assert_eq!(attr_created.items.len(), 5);
        assert!(attr_created.total_pages > 0);
    }
}
