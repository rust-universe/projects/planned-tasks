mod attribute_controller;
pub(crate) mod attribute_model;

use actix_web::web;

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/attribute")
            .service(attribute_controller::create)
            .service(attribute_controller::fetch_all)
            .service(attribute_controller::update)
            .service(attribute_controller::fetch)
            .service(attribute_controller::delete)
            .service(attribute_controller::upsert),
    );

    cfg.service(
        web::scope("/attribute/value")
            .service(attribute_controller::create)
            .service(attribute_controller::fetch_all)
            .service(attribute_controller::update)
            .service(attribute_controller::fetch)
            .service(attribute_controller::delete)
            .service(attribute_controller::upsert),
    );

    cfg.service(
        web::scope("/attribute/value/validation")
            .service(attribute_controller::fetch)
            .service(attribute_controller::fetch_all)
            .service(attribute_controller::create)
            .service(attribute_controller::update)
            .service(attribute_controller::upsert),
    );
}
