use chrono::{DateTime, Utc};
use schema::attribute::Attribute;
use schema::attribute::AttributeValueDataType;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AttributeModel {
    pub id: i32,
    pub display_text: String,
    pub description: Option<String>,
    pub data_type: AttributeValueDataType,
    pub is_template: bool,
    pub created_at: DateTime<Utc>,
    pub updated_at: DateTime<Utc>,
}

impl From<Attribute> for AttributeModel {
    fn from(attr: Attribute) -> Self {
        AttributeModel {
            id: attr.id,
            display_text: attr.display_text,
            description: attr.description,
            data_type: attr.data_type,
            is_template: attr.is_template,
            created_at: attr.created_at,
            updated_at: attr.updated_at,
        }
    }
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct NewAttributeModel {
    pub display_text: String,
    pub description: Option<String>,
    pub data_type: AttributeValueDataType,
}
