mod controller;

use actix_web::web;

pub fn configure(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::scope("/task")
            .service(controller::fetch)
            .service(controller::fetch_all)
            .service(controller::create)
            .service(controller::update)
            .service(controller::upsert),
    );
}
