use schema::task::Task;

use actix_web::{delete, get, post, put, web, HttpResponse, Responder};
use chrono::Utc;

#[get("/{task_id}")]
pub async fn fetch(path: web::Path<i32>) -> impl Responder {
    let task_id = path.into_inner();

    web::Json(Task {
        id: task_id,
        title: "test".to_string(),
        description: "description".to_string(),
        created_at: Utc::now(),
        updated_at: Utc::now(),
    })
}

#[get("/")]
pub async fn fetch_all() -> impl Responder {
    HttpResponse::Ok().json("hello")
}

#[post("/")]
pub async fn create() -> impl Responder {
    HttpResponse::Ok().body("Created")
}

#[put("/")]
pub async fn update() -> impl Responder {
    HttpResponse::Ok().body("Updated")
}

#[delete("/{task_id}")]
pub async fn delete(_path: web::Path<i32>) -> impl Responder {
    HttpResponse::Ok().body("Deleted")
}

#[post("/upsert")]
pub async fn upsert() -> impl Responder {
    HttpResponse::Ok().body("Upsert")
}

#[cfg(test)]
mod tests {
    use actix_web::{test, App};
    use schema::task::Task;

    #[actix_rt::test]
    async fn test_fetch_task_details() {
        let app = test::init_service(App::new().service(super::fetch)).await;

        // Create request object
        let req = test::TestRequest::with_uri("/123").to_request();

        // Call application
        let resp = test::call_service(&app, req).await;
        assert!(resp.status().is_success());

        let task: Task = test::read_body_json(resp).await;
        assert_eq!(123, task.id);
        assert_eq!("test", task.title);
        assert_eq!("description", task.description);
    }
}
