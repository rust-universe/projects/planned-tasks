pub fn default_i64_1() -> i64 {
    1i64
}

pub fn default_u64_1() -> u64 {
    1u64
}

pub fn default_empty_vec<T>() -> Vec<T> {
    vec![]
}
