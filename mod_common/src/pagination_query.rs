use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Clone, Copy)]
#[serde(rename_all = "camelCase")]
pub enum SortType {
    Asc,
    Desc,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PaginationQuery {
    #[serde(default = "crate::serde_macro_defaults::default_u64_1")]
    pub page: u64,

    #[serde(default = "default_u64_10")]
    pub per_page: u64,

    pub order_by: Option<String>,
    pub sort_by: Option<String>,

    #[serde(default = "default_sort_type")]
    pub sort_type: SortType,
}

/// Default value to use for per-page attribute
pub fn default_u64_10() -> u64 {
    10u64
}

pub fn default_sort_type() -> SortType {
    SortType::Desc
}

pub fn default_pagination_query() -> PaginationQuery {
    PaginationQuery {
        page: 0,
        per_page: 10,
        sort_type: SortType::Desc,
        order_by: None,
        sort_by: None,
    }
}
