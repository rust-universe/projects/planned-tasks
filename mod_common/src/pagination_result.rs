use crate::pagination_query::{self, PaginationQuery};
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
pub struct PaginationResult<T> {
    #[serde(default = "crate::serde_macro_defaults::default_empty_vec::<T>")]
    pub items: Vec<T>,

    #[serde(default = "pagination_query::default_pagination_query")]
    pub query: PaginationQuery,

    #[serde(default = "crate::serde_macro_defaults::default_i64_1")]
    pub total_pages: i64,
}
