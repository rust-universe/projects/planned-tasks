# Planned Tasks

Planned Tasks is a software created for task / issue tracking.

[![pipeline status](https://gitlab.com/rust-universe/projects/planned-tasks/badges/main/pipeline.svg)](https://gitlab.com/rust-universe/projects/planned-tasks/-/commits/main)
[![codecov](https://codecov.io/gl/rust-universe:projects/planned-tasks/branch/main/graph/badge.svg?token=QLBOZRJUO5)](https://codecov.io/gl/rust-universe:projects/planned-tasks)
[![Built with cargo-make](https://sagiegurari.github.io/cargo-make/assets/badges/cargo-make.svg)](https://sagiegurari.github.io/cargo-make)

## Cargo Make

### Installing cargo-make

We are using `cargo-make` package to automate the installing-dependencies, building and running an application.
It also allows us to execute the commands/code on pre-commit or pre-push hooks.

`cargo install --no-default-features --force cargo-make`

## Debian 11 / RHEL 9 Linux Setup

### Install required packages to build

`cargo make install_dependencies`

## Building & Running an Application

- ### Get to PostgreSQL / `psql` shell

  Use command `sudo -i -u postgres psql` to get to
  PostgreSQL / psql shell.

  If above command is not working on your distribution then consult with
  your distribution specific documentation.

- ### Create PostgreSQL DB and role / user

  Main Database

  ```sql
  create user planner with password 'gSm&AP3Ci#u$dm';
  create database planned_tasks owner planner;
  ```

  Database for Running Unit-Tests

  ```sql
  create database planned_tasks_tests owner planner;
  ```

- ### Setting database specific environment variables

  To start an application we need environment variables listed below:

  ```bash
  export DB_USER=planner
  export DB_PASSWORD=gSm&AP3Ci#u$dm
  export DB_NAME=planned_tasks
  export DB_HOST=localhost
  ```

  To run test-cases we need to set above environment variables but update the `DB_NAME` environment variable to point to test-cases specific DB:

  ```bash
  export DB_NAME=planned_tasks_tests
  ```

- ### Build and / or Run an application

  Command to run an application

  ```bash
  cargo make run
  ```

  Command to run an application (Auto-restart server when you change any file)

  ```bash
  cargo make runw
  ```

  Command to run test-cases

  ```bash
  cargo make check_build_and_test
  ```

  Command to format files

  ```bash
  cargo make format
  ```

  Command to generate HTML coverage report (test-cases will be executed to generate the report)

  ```bash
  cargo make coverage_html
  ```

  To clean files generated during build process or test execution

  ```bash
  cargo make clean_all
  ```

  > IMP Production Note: Use below command to run the server, to avoid request drop and ADDRINUSE error

  ```
  catflap -- cargo watch -x "run --package api_server --bin api_server"
  ```

- ### Login to `psql` shell using new user

  Once user is created you can login to `psql` shell directly using:

  To login to application DB

  ```bash
  psql planned_tasks -U planner -h 127.0.0.1
  ```

  To login to DB which is used to run the test-cases

  ```bash
  psql planned_tasks_tests -U planner -h 127.0.0.1
  ```

- ### Auto-Reloading Development Server

  Use below command to start an application-server in watch mode.

  1. Build an application using cargo build command and keep watching for changes

  ```bash
  cargo make runw
  ```

  2. Build an application in release mode and keep watching for changes

  ```bash
  cargo make runw_release
  ```

  **OR**

  Install Cargo-Watch crate which can be used to restart/rebuild an application on changes.

  ```bash
  cargo install cargo-watch
  ```

  Use below command to start an application-server in watch mode.

  ```bash
  cargo watch -x "run --package api_server --bin api_server"
  ```

- ### Enable HTTPS Support

  #### Install your OS specific package from below list:

  ```bash
  # MacOS
  brew install openssl@1.1

  # Linux
  sudo port install openssl
  sudo pkgin install openssl
  sudo pacman -S pkg-config openssl
  sudo apt-get install pkg-config libssl-dev
  sudo dnf install pkg-config openssl-devel
  ```

  #### Generate self-signed certificate:

  ```bash
  openssl req -x509 -newkey rsa:4096 -nodes -keyout key.pem -out cert.pem -days 365 -subj '/CN=localhost'
  ```

  #### Setting environment variables to run application over https

  Set path of the file generated in above command as the environment variables

  ```bash
  HTTPS_PRIVATE_KEY=<SELF_SIGNED_PRIVATE_KEY_FILE_PATH>
  HTTPS_CERT=<SELF_SIGNED_CERTIFICATE_FILE_PATH>
  ```

  > **Note:** Certificate and private key has to be in PEM format, otherwise server will panic.

- ### Generate Schema File

  When you build or run an application using cargo-make, schema file will be generated automatically for you.

  If you want to generate the schema file manually then you can use the below command to do so.

  ```bash
  cd diesel_schema
  printf '#![allow(unused)]\n#![allow(clippy::all)]\n\n' > src/schema.rs && diesel print-schema >> src/schema.rs
  ```

  Once schema is generated you mostly have to edit the schema.rs file to use the enum type.
  Places needs correction can be identified by simply running `cargo build`

  > Note: Make sure to modify the `Makefile.toml` if you add new enum in diesel-schema crate, This will help in generating correct schema file when we run cargo-make command.

## FAQ

1. **How to resolve `use of undeclared crate or module tasks` for struct which derives Diesel specific macros?**\
   Common type of error for this scenario
   ```
   error[E0433]: failed to resolve: use of undeclared crate or module `tasks`
    --> diesel_schema/src/planner/task.rs:5:12
     |
   5 | pub struct Task {
     |            ^^^^ use of undeclared crate or module `tasks`
   ```
   You can resolve this error by simply using the schema module in your file
   ```rust
   use crate::schema::*;
   ```

## License

This software is licensed under the [Apache-2.0](LICENSE)
